module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
	grunt.initConfig({
		express: {
			build: {
				options : {
					server: ('js/server.js'),
					port: 8080,
					// hostname: "192.168.1.65",
				}
			}
		},
		jshint: {
			files: ['Gruntfile.js', 'server.js'],
			options: {
				globals: {
					jQuery: true
				}
			}
		},
	});
	grunt.registerTask('build', [ 'jshint', 'express', 'express-keepalive']);
	grunt.registerTask('default', ['build']);
};