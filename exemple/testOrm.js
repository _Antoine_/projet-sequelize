/**
**  create ssh tunel between this computer and raspberry and do mysql transaction into it
**  ssh -NfL 8080:localhost:3306 root@176.189.130.29
**/

var data = require("./data.json");
var Sequelize = require("sequelize");

console.log("uri : "+'mysql://'+data.user+':'+'$password'+'@'+data.ip+':'+data.port+'/'+data.db);

var co = connection();
var user = createTableUser();

// user.create({nom: 'roux', prenom: 'antoine', mail: 'antoinroux@homail.fr'});

// query****
user.findById('1').then(function(u) {
  console.log('findById : '+u.nom)
});

user.findOne({ where: {prenom: 'antoine', 'nom': 'roux'} }).then(function(u) {
  console.log("findOne : "+u.mail);
});

user.findOrCreate({
  where: {
    nom: 'admin'
  },
  defaults: {
    prenom: 'admin',
    mail: 'admin@yopmail.com'
  }
}).spread(function() {
  console.log("findOrCreate");
});

user.findAndCountAll({
  where: {
    $or: {
      nom: 'roux',
      mail: 'admin@yopmail.com'
    }
  }
}).then(function(result) {
  console.log("count : "+result.count);
  for(it in result.rows) {
    console.log('findAndCountAll : result.id : '+result.rows[it].id);
  }
});

user.findAll({order: 'id', raw: true }).then(function(result) {
  for(it in result) {
    console.log('findAll : result : '+result[it].nom);
  }
});

user.count().then(function(nbUser) {
  console.log("Il y'a "+nbUser+" user"+(nbUser > 1 ? "s": "") +" dans la base.");
});




//************************************************************ function **************************************************//
function createTableUser() {
  var user = co.define('user', {
    id : {
      type : Sequelize.INTEGER,
      autoIncrement : true,
      primaryKey : true,
      unique : true
    },
    nom : {type : Sequelize.STRING},
    prenom : {type : Sequelize.STRING},
    mail : {
      type : Sequelize.STRING,
      allowNull : false,
      validate: {
        isEmail: true
      }
    }
  });
  user.sync({force : false}).done(function() {
    console.log("sync for apply model and create table if not exists : succeed");
  }, function () {
    console.log("sync : faillure !!!");
  });
  return user;
}

function connection() {
  var co = new Sequelize('mysql://'+data.user+':'+data.password+'@'+data.ip+':'+data.port+'/'+data.db);
  // var co = new Sequelize(data.db, data.user, data.password, {
  //   host: '127.0.0.1',
  //   port: '8080',
  //   dialect: 'mysql',
  //   pool: {
  //     max: 5,
  //     min: 0,
  //     idle: 10000
  //   }
  // });
  return co;
}
