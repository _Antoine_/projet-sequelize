$(function() {
	$("#publis").click(function() {
		$("content").load("/views/publis.html", function() {
			$.get({
			url: "/views/publis.html",
			success: function(data) {
				$("content").html(data);
				$.get({
					url:"/publis/",
					success: function(r) {
						r.forEach(function(t) {
							var li = $("<li>");
							li.append("id : "+t.id + " - titre : " + t.titre + " - descr : " + t.description + " - date : " + t.date);
							$("content > ul").append(li);
						});
					}
				});
			},
		});

			console.log("publis total");
		});
	});

	$("#publisFrag").click(function() {
		$("content").load("/views/publisFrag.html", function() {
			$.get({
				url:"/publisInf/",
				success: function(p) {
					console.log(p);
					p.forEach(function(t) {
						var li = $("<li>");
						li.append("id : "+t.id + " - titre : " + t.titre + " - descr : " + t.description + " - date : " + t.date);
						$(".left > .frag").append(li);
					});
				}
			});
			$.get({
				url:"/publisSup/",
				success: function(p) {
					console.log(p);
					p.forEach(function(t) {
						var li = $("<li>");
						li.append("id : "+t.id + " - titre : " + t.titre + " - descr : " + t.description + " - date : " + t.date);
						$(".right > .frag").append(li);
					});
				}
			});
			console.log("public fragmenté");
		});
	});

	$("#chercheurs").click(function() {
		$.get({
			url: "/views/chercheurs.html",
			success: function(data) {
				$("content").html(data);
				$.get({
					url:"/chercheurs/",
					success: function(r) {
						r.forEach(function(t) {
							var li = $("<li class='chercheurs clikeable'>");
							li.append("<id>"+t.num + "</id> - <info class='info'>"+t.prenomNom+" - "+t.mail+"</info><img class='del' src='/res/del.png'></img>");
							$("content > div > ul").append(li);
						});
						
						$(".del").click(function() {
							var id = $(this).prev().prev("id").html()

							$.ajax({
								type: "delete",
								url: "/chercheurs/",
								data: {
									id: id,
								},
								success: function() {
									$("#chercheurs").click();
								}
							});
						});

						$(".chercheurs").click(function() {
							$(".selected").removeClass("selected");
							$(this).addClass("selected");
						});
						
						$(".query").click(function() {
							var id = $(".selected > id").html();
							var date = $("#inputDate").val();
							console.log(id);
							console.log(date);
							if(date && id) {
								$.get({
									url: "/publis/"+id+"/"+date+"/",
									success: function(r) {
										r.forEach(function(t) {
											var li = $("<li>");
											li.append(t.id + " - " + t.titre + " - " + t.description + " - " + t.date);
											$(".res").append(li);
										});
									},
									error: function() {
										$(".res").append("<p class='error'>Error : No data found into database for this rest query : '/publis/"+id+"/"+date+"/' </p>")
									},
								});
							}
						});

						$(".buttonUser").click(function() {
							if($("#inputUserId").val() && $("#inputUserNom").val() && $("#inputUserPrenom").val() && $("#inputUserMail").val()) {
								$.post({
									url: "/chercheurs/",
									data: {
										num: $("#inputUserId").val(),
										prenomNom: $("#inputUserPrenom").val()+" "+$("#inputUserNom").val(),
										mail: $("#inputUserMail").val(),
									},
									success: function() {
										$("#chercheurs").click();
									}
								});
							}

						});
							
					}

				});
			},
		});
	});
	
});