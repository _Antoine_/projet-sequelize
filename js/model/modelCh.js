module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Chercheurs', {
		num: {
		    type: DataTypes.INTEGER, 
		    primaryKey: true
		},
		prenomNom: {
			type: DataTypes.TEXT
		},
		mail: {
			type: DataTypes.TEXT
		}
	},{
	  	freezeTableName: true, // Model tableName will be the same as the model name
	  	timestamps: false
	})
}