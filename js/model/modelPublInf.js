module.exports = function(sequelize, DataTypes) {
	return sequelize.define('PublicationsInf', {
		id: {
		    type: DataTypes.INTEGER, 
		},
		titre: {
			type: DataTypes.TEXT
		},
		description: {
			type: DataTypes.TEXT
		},
		date: {
			type: DataTypes.INTEGER
		},
		numC: {
			type: DataTypes.INTEGER
		}
	},{
	  	freezeTableName: true, // Model tableName will be the same as the model name
	  	timestamps: false
	})
}
