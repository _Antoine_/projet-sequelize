module.exports = function(sequelize, DataTypes) {
	return sequelize.define('PublicationsSup', {
		id: {
		    type: DataTypes.INTEGER, 
		    primaryKey: true
		},
		titre: {
			type: DataTypes.TEXT
		},
		description: {
			type: DataTypes.TEXT
		},
		date: {
			type: DataTypes.INTEGER
		},
		numC: {
			type: DataTypes.INTEGER
		}
	},{
	  	freezeTableName: true, // Model tableName will be the same as the model name
	  	timestamps: false
	})
}
