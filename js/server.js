/*
	Mickael Bettinelli
	Antoine Roux
*/

var confdir = "../conf/";

var express = require("express");
var sequelize = require("sequelize");
var data = require(confdir+"/data.json");
var bodyParser = require("body-parser");
var path = require("path");


var server = function() {
	var urlLyon = 'postgres://'+data.user+':'+data.password+'@'+data.ip+':'+data.port+'/Lyon';
	var urlValence = 'postgres://'+data.user+':'+data.password+'@'+data.ip+':'+data.port+'/Valence';

	this.Lyon = new sequelize(urlLyon);
	this.Valence = new sequelize(urlValence);
};

server.prototype.init = function() {
	self = this;			// use into express road

	this.app = module.exports = express();
	this.app.use("/js", express.static(__dirname));
	this.app.use("/views", express.static(__dirname+"/../views/"));
	this.app.use("/res", express.static(__dirname+"/../res/"));
	
	this.app.use(bodyParser.json());
	this.app.use(bodyParser.urlencoded({
		extended: true
	}));
	this.fraged = false;
};

server.prototype.route = function() {

	this.app.get('/', function(req, res) {
		// default road
		res.sendFile(path.resolve("index.html"));
	});
	
	this.app.delete("/chercheurs/", function(req, res) {
		var id = req.body["id"];

		var Chercheurs = self.Lyon.import(__dirname +"/model/modelCh.js");
		Chercheurs.findById(id).then(function(u) {
			var nb = u.destroy();
			console.log(nb);
		});

		res.writeHead(200);
		res.send();
	});

	// curl --data "num=6&nomPrenom=jeanHeud&mail=jeanHeud@gmail.com" 127.0.0.1/chercheurs
	this.app.post('/chercheurs/', function(req, res) {
		var num = req.body.num;
		var prenomNom = req.body.prenomNom;
		var mail = req.body.mail;

		var Chercheurs = self.Lyon.import(__dirname +"/model/modelCh.js");

		Chercheurs.sync().then(function() {
			var data = {
				num: num,
				prenomNom: prenomNom,
				mail: mail,
			};

			Chercheurs.create(data).then(function(p){
				console.log(p.get());
				res.writeHead(200);
				res.send();
			})
		}).catch(function(error) {
			res.writeHead(300);
			res.send();
			console.log(error);
		});	
	});

	this.app.get('/chercheurs/', function(req, res) {
		var Chercheurs = self.Lyon.import(__dirname +"/model/modelCh.js");
		
		//Event handling
		Chercheurs.sync().then(function() {
			Chercheurs.findAll({
				attributes: ['num', 'prenomNom', 'mail']
			}).then(function(r) {
				var liste = [];
				r.forEach(function(t){
					liste.push(t.dataValues);
				})
				res.send(liste);
			});
		}).catch(function(error) {
			console.log(error);
		});	
	});

	this.app.get('/publis/', function(req, res) {
		var Publications = self.Valence.import(__dirname +"/model/modelPubl.js");
				
		Publications.sync().then(function() {
			Publications.findAll({
				attributes: ['id', 'titre', 'description', 'date', 'numC']
			}).then(function(r) {
				var liste = [];
				r.forEach(function(t){
					liste.push(t.dataValues);
				})
				res.send(liste);
			});
		}).catch(function(error) {
			console.log(error);
		});
	});

	this.app.get('/publis/:id/:date/', function(req, res) {
		var num = req.params.id;
		var date = req.params.date;
		
		var Publications;

		// make classic query
		// self.Lyon.query('SELECT * FROM "Chercheurs"', { model: Chercheurs}).then(function(p){
		// 	console.log(p);
		// });

		if(date <= 2010) {
			Publications = self.Lyon.import(__dirname +"/model/modelPublInf.js");
		}
		else {
			Publications = self.Valence.import(__dirname +"/model/modelPublSup.js");
		}
		
		Publications.sync().then(function() {
			Publications.findAll({
				attributes: ['id', 'titre', 'description', 'date', 'numC'],
				where: {
					numC: num,
					date: date,
				}
			}).then(function(r) {
				var liste = [];
				
				if(!r.length) {
					res.status(500).send();
				}
				else {
					r.forEach(function(t){
						liste.push(t.dataValues);
					});
					res.status(200).send(liste);
				}
			});
		}).catch(function(error) {
			res.status(500).send();
		});
	});


	this.app.get('/publisInf/', function(req, res) {
		var Publications = self.Valence.import(__dirname +"/model/modelPubl.js");
		var PublicationsInf = self.Lyon.import(__dirname +"/model/modelPublInf.js");
		
		PublicationsInf.count().then(function(){
			PublicationsInf.findAll().then(function(r) {
				var data = [];
				r.forEach(function(t){
					data.push(t);
				});
				res.send(data);
			}).catch(function(error) {
				res.send();
			});
		}).catch(function(error) {
			//Si la table n'existe pas
			Publications.sync().then(function() {
				//Inférieur à 2010
				Publications.findAll({
					attributes: ['id', 'titre', 'description', 'date', 'numC'],
					where: { 
						date: {
							lt: 2010,
						},
					}
				}).then(function(r) {
					var data = [];
					PublicationsInf.sync().then(function(){
						r.forEach(function(t){
							PublicationsInf.create(t.dataValues);
							data.push(t.dataValues);
						});
						res.send(data);
					}).catch(function(error) {
						res.send();
					});
				});
			}).catch(function(error) {
				res.send();
			});
		});
	});

	this.app.get('/publisSup/', function(req, res) {
		var Publications = self.Valence.import(__dirname +"/model/modelPubl.js");
		var PublicationsSup = self.Valence.import(__dirname +"/model/modelPublSup.js");

		PublicationsSup.count().then(function(){
			//On affiche les valeurs, la table existe
			PublicationsSup.findAll().then(function(r) {
				var data = [];
				PublicationsSup.sync().then(function(){
					r.forEach(function(t){
						data.push(t);
					})
					res.send(data);
				}).catch(function(error) {
					res.send();
				});
			});
		}).catch(function(error) {
			Publications.sync().then(function() {
				//La table n'existe pas
				//Supérieur ou égal à 2010
				Publications.findAll({
					attributes: ['id', 'titre', 'description', 'date', 'numC'],
					where: { 
						date: {
							gte: 2010,
						},
					}
				}).then(function(r2) {
					PublicationsSup.sync().then(function() {
						var data = [];
						r2.forEach(function(t2) {
							PublicationsSup.create(t2.dataValues);
							data.push(t2.dataValues);
						});
						res.send(data);	
					}).catch(function(error) {
						res.send();
					});
				});
			}).catch(function(error) {
				res.send();
			});
		})
	});
};

server.prototype.check = function() {
	this.Lyon.authenticate().then(function(error) {
		console.log(error);
	});

	this.Valence.authenticate().then(function(error) {
		console.log(error);
	});
};



var s = new server();
s.init();
s.check();
s.route();
